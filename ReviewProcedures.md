# Review Procedures

### General

For every review two reviewers take responsibility:

- Circle: Two GNOME Circle Committee members.
- Core and Development: One Release Team member, one Design Team member.

However, every member of the responsible teams can contribute to the review. As per the [Foundation's Software Policy](https://wiki.gnome.org/Foundation/SoftwarePolicy), the Release Team has the final say about Core apps and Development tools.

1.  Two reviewers comment in the issue that they will be responsible for the review.
2.  Assessment of foundations: Check if fundamental requirements like license, reliable maintainership, and technical foundations are met. If not, proceed to the last step, skipping the detailed review.
3.  If the project only requires some adjustments or the reviewers have suggestions for the app, those are filled in the apps issue tracker with a remark like "Part of *\<Category\>* review."
4.  The [App Criteria](AppCriteria.md) checklist is copied and filled out for the preparation of the report. It should be included in the report as well.
5.  The responsible reviewers compile a review, going through all criteria.
    - The reviewers can use [a non-public pad](https://hedgedoc.gnome.org/) to compile the review.
6.  The report is posted with one of the following conclusions:
    - Initial review (new apps): Approved, needs adjustments, rejected
    - Regular review: No recommendations, recommendations, slated for removal (see app removal below)
    - Irregular review: Recommendations, slated for removal (see app removal below)

If an app is added or removed, please follow the checklist at the end of this document.

### Regular Reviews

All apps in GNOME Core, Development, and Circle are regularly reviewed for quality control. Those reviews are foremost to help the maintainers with finding potential problems within their project, as well as finding out how they could be resolved.

The review is based on the applicable [App Criteria](AppCriteria.md). We are currently targeting one review per app every two years.

### Irregular review

Apps can be partially or completely reviewed at any point in time if there are reports about severe problems like data loss caused by the app or dramatic changes to the app that may make it no longer eligible for the category it is currently in.

If it becomes clear that a prompt response from the app maintainers is required, a tracking issue is created. Otherwise, the issues can be reported in the app's issue tracker and be revisited for the next regular review.

### Stock responses

Acceptance into Circle:

> Dear *\<name\>*, thanks for submitting your app to GNOME Circle\! It might happen that Circle Committee members will fill issues in your app's issue tracker before the final review. You can either act on those filled issues already or wait for the complete review. The complete review will be posted later in this issue. We are looking forward to exploring your app\!

- Completely fulfills [Circle App Criteria](AppCriteria.md)

### Checklist for app status changes

- [ ] Update the respective list for Core/Development/Circle apps
- [ ] Move new apps to `GNOME/` (Core/Development) or `Incubator/` (Incubation)
- [ ] For new Core/Development/Incubation apps, add `org.gnome.<codename>` to [data/registered-app-ids.yml](data/registered-app-ids.yml)
- [ ] Open an issue against [Damned Lies](https://gitlab.gnome.org/Infrastructure/damned-lies/-/issues/new) to move the app to the correct release set. (Does not apply to all Circle apps.)
- [ ] Create a merge request against Software to update the list of featured apps
- [ ] Announce new apps on "This Week in GNOME"
- [ ] Update the [help page category](https://gitlab.gnome.org/Infrastructure/help.gnome.org/-/blob/master/APPS.yaml). (Not all apps have help pages.)
